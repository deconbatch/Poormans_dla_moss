/**
 * Poor mans DLA Moss.
 * ref. https://www.deconbatch.com/2019/10/the-poor-mans-dla-diffusion-limited.html
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2021.03.27
 */

void setup(){
  size(980, 980);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  rectMode(CENTER);
  smooth();
  noLoop();
}
 
void draw(){

  int   fileMax   = 3;
  float distStart = random(0.01);

  translate(width * 0.5, height * 0.5);
  for (int fileCnt = 0; fileCnt < fileMax; fileCnt++) {

    float distBase = 0.01 + (distStart + fileCnt * 0.003) % 0.01;
    int   shotMax  = floor(map(distBase, 0.01, 0.02, 3000.0, 1000.0));
    int   runMax   = floor(max(width, height) * 0.5);
    
    background(0.0, 0.0, 90.0, 100.0);
    drawNodes(dlaResult(shotMax, runMax, distBase), runMax * distBase);
    casing();

    saveFrame("frames/" + String.format("%04d", fileCnt) + ".png");

  }
  exit();

}

/**
 * dlaResult : get poor man's dla result.
 */
private ArrayList<Node> dlaResult(int _shotMax, int _runMax, float _distBase) {

  float angleMult  = random(0.05, 1.05);
  float anglePhase = random(TWO_PI);
  float angleWidth = random(HALF_PI, TWO_PI);
  float nHue       = random(360.0);
  float nDist      = _distBase * _runMax;

  ArrayList<Node> nodes = new ArrayList<Node>();
  nodes.add(new Node(0.0, 0.0, nHue)); // seed

  float angle = 0.0;
  for (int shotCnt = 0; shotCnt < _shotMax; shotCnt++) {

    float shotRatio = map(shotCnt, 0, _shotMax, 0.0, 1.0);
    angle += angleMult * sin(anglePhase + angleWidth * shotRatio);

    // start from outer rim
    Node n = new Node(
                      _runMax * cos(angle),
                      _runMax * sin(angle),
                      nHue
                      );
    for (int runCnt = 0; runCnt < _runMax; runCnt++) {
      // run to the center
      n.x -= cos(angle);
      n.y -= sin(angle);

      if (checkCollision(nodes, n, nDist)) {
        nodes.add(n);
        nHue += _distBase * 10.0;
        break;
      }
      
    }

  }

  return nodes;

}

/**
 * checkCollision : check collision between a node and the nodes.
 */
private boolean checkCollision(ArrayList<Node> _nodes, Node _n, float _dist) {

  // run through if there were one very near node
  for (Node n : _nodes) {
    if (dist(n.x, n.y, _n.x, _n.y) < _dist * 0.7) {
      return false;
    }
  }

  // collision
  for (Node n : _nodes) {
    if (dist(n.x, n.y, _n.x, _n.y) < _dist) {
      return true;
    }
  } 

  return false;
}

/**
 * drawNodes : draw the lines between nodes.
 */
private void drawNodes(ArrayList<Node> _nodes, float _dist) {
  float eSat = 60.0;
  float eBri = 40.0;
  noFill();
  for (Node n : _nodes) {
    for (Node p : _nodes) {
      float d = dist(n.x, n.y, p.x, p.y);
      if (d <= _dist) {
        strokeWeight(d * d * d * 0.1 / _dist);
        stroke(p.h % 360.0, eSat, eBri, 100.0);
        line(n.x, n.y, p.x, p.y);
      }
    }
  }
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  blendMode(BLEND);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}

/**
 * Node : Hold a node information.
 */
private class Node {
  // informations are writable purposely
  public float x, y; // coordinate value of the node.
  public float h;    // hue value of the node.

  Node(float _x, float _y, float _hue) {
    x = _x;
    y = _y;
    h = _hue;
  }
}
